// specfileserver project main.go
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
)

type SuccessMessage struct {
	Success  bool   `json:"success"`
	Error    string `json:"error"`
	ErrorMsg string `json:"errormsg"`
}

func fileexistsHandler(w http.ResponseWriter, req *http.Request) {
	fileInput := req.FormValue("filename")
	filename := fileInput + ".json"
	fileExists := false
	if _, err := os.Stat(filename); err == nil {
		fileExists = true
	}
	type FileExistsMessage struct {
		FileExists bool `json:"fileexists"`
	}
	out, _ := json.Marshal(FileExistsMessage{fileExists})
	w.Header().Set("Content-Type", "application/json")
	w.Write(out)
}

func saveJSONFileHandler(w http.ResponseWriter, req *http.Request) {
	JSONData := req.FormValue("data")
	fileInput := req.FormValue("filename")
	fileName := fileInput + ".json"

	w.Header().Set("Content-Type", "application/json")
	var out []byte
	f, err := os.Create(fileName)
	if err != nil {
		out, _ = json.Marshal(SuccessMessage{false, err.Error(), "filecreate"})
		w.Write(out)
		return
	}
	_, err = f.WriteString(JSONData)
	if err != nil {
		out, _ = json.Marshal(SuccessMessage{false, err.Error(), "writestring"})
		w.Write(out)
		return
	}
	f.Close()
	out, _ = json.Marshal(SuccessMessage{true, "", ""})
	w.Write(out)
}

func loadJSONHandler(w http.ResponseWriter, req *http.Request) {
	fileName := req.FormValue("fileName")
	w.Header().Set("Content-Type", "application/json")
	JSONText, err := ioutil.ReadFile(fileName)
	if err != nil {
		out, _ := json.Marshal(SuccessMessage{false, err.Error(), "fileread"})
		w.Write(out)
		return
	}
	w.Write(JSONText)
}

func fileAutoCompleteHandler(w http.ResponseWriter, req *http.Request) {
	inputString := req.FormValue("searchString")
	searchString := "fileDB/" + inputString + "*"
	w.Header().Set("Content-Type", "application/json")
	matches, err := filepath.Glob(searchString)
	if err != nil {
		out, _ := json.Marshal(SuccessMessage{false, err.Error(), "fileAutoComplete"})
		w.Write(out)
		return
	}

	type FilesFoundMessage struct {
		FilesFound  []string `json:"filesFound"`
		IsDirectory []bool   `json:"isDirectory"`
	}
	var fullPathresults []string
	if len(matches) >= 10 {
		fullPathresults = matches[0:10]
	} else {
		fullPathresults = matches
	}
	var filesFound []string
	var isDir []bool
	for i := 0; i < len(fullPathresults); i++ {
		fullPath := filepath.ToSlash(fullPathresults[i])
		ext := filepath.Ext(fullPath)
		if ext == "" {
			isDir = append(isDir, true)
		} else {
			isDir = append(isDir, false)
		}

		fileName := fullPath[7 : len(fullPath)-len(ext)]
		filesFound = append(filesFound, fileName)
	}

	out, _ := json.Marshal(FilesFoundMessage{filesFound, isDir})
	w.Write(out)
}

func main() {
	http.HandleFunc("/webservice2/fileexists", fileexistsHandler)
	http.HandleFunc("/webservice2/saveJSONFile", saveJSONFileHandler)
	http.HandleFunc("/webservice2/loadJSON", loadJSONHandler)
	http.HandleFunc("/webservice2/fileAutoComplete", fileAutoCompleteHandler)
	http.Handle("/3rdParty/", http.StripPrefix("/3rdParty/", http.FileServer(http.Dir("3rdParty"))))
	http.Handle("/js/", http.StripPrefix("/js/", http.FileServer(http.Dir("js"))))
	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("css"))))
	http.Handle("/html/", http.StripPrefix("/html/", http.FileServer(http.Dir("html"))))

	err := http.ListenAndServe("localhost:7000", nil)
	if err != nil {
		fmt.Print("server error")
	}

}
