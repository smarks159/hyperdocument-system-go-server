# Hyperdocument System Server #

This is a simple http server written to support my hyperdocument system. See [the wiki](https://github.com/smarks159/hyperdocument-system-wiki) for a description of the project.

Currently, the server is very simple, each document in the system is stored in a JSON file under the fileDB folder where the server executable is located.

By default, the server is setup to listen on port 7000.

To use with apache see the [following instructions](https://github.com/smarks159/hyperdocument-system-go-server/wiki/apache_setup)
